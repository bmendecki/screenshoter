### QuickScreenshoter ###

Functions:

- takes screenshots from list of urls (from file)
- saves screenshots in folder as .png
- creates names of pngs from urls 

Usage:

- put urls in file 'Text-16.txt' :) one by one
- fire phantomjs screens.js